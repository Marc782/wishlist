import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinosViajesActionTypes, NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Actions} from '@ngrx/store-devtools/src/reducer';

// EFFECTS

@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) { }
}
